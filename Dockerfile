FROM hseeberger/scala-sbt

LABEL maintainer="Bartashuk Pavel <bartashuk.pavel@e-queo.com>"

EXPOSE 8888

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

VOLUME [ "/usr/src/app" ]

CMD ["sbt", "run"]
