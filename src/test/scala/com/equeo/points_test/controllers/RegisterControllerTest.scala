package com.equeo.points_test.controllers

import com.equeo.points_test.AuthService
import com.equeo.points_test.domain.User
import com.equeo.points_test.exceptions.UserAlreadyRegisteredException
import com.equeo.points_test.services.{TokenService, UserService}
import com.twitter.finagle.http.Status
import com.twitter.finatra.http.{EmbeddedHttpServer, HttpTest}
import com.twitter.inject.server.FeatureTest
import org.scalamock.scalatest.MockFactory

class RegisterControllerTest extends FeatureTest with MockFactory with HttpTest {

  val userService: UserService = mock[UserService]
  val tokenService: TokenService = mock[TokenService]

  override protected val server: EmbeddedHttpServer =
    new EmbeddedHttpServer(new AuthService)
      .bind[UserService](userService)
      .bind[TokenService](tokenService)

  test("success registration") {
    (userService.register _)
      .expects(User(0, "test", "password"))
      .returns(User(5, "test", "password"))
    (tokenService.generateAndSaveToken _)
      .expects(5)
      .returns("pxzu382pdshv9z")

    server.httpPost(path = "/auth/register", postBody =
      """
        |{
        |   "username": "test",
        |   "password": "password"
        |}
      """.stripMargin, andExpect = Status.Ok, withJsonBody =
      """
        |{
        |   "success": true,
        |   "response":
        |   {
        |     "user": {
        |       "id": 5,
        |       "username": "test"
        |     },
        |     "token": "pxzu382pdshv9z"
        |   }
        |}
      """.stripMargin)
  }

  test("registration with already registered username") {
    (userService.register _)
      .expects(User(0, "test", "password"))
      .returns(User(5, "test", "password"))
    (tokenService.generateAndSaveToken _)
      .expects(5)
      .returns("pxzu382pdshv9z")

    server.httpPost(path = "/auth/register", postBody =
      """
        |{
        |   "username": "test",
        |   "password": "password"
        |}
      """.stripMargin, andExpect = Status.Ok)

    (userService.register _)
      .expects(User(0, "test", "password"))
      .throwing(new UserAlreadyRegisteredException("err description from test"))

    server.httpPost(path = "/auth/register", postBody =
      """
        |{
        |   "username": "test",
        |   "password": "password"
        |}
      """.stripMargin, andExpect = Status.BadRequest, withJsonBody =
      """
        |{
        |   "success": false,
        |   "error": {
        |     "code": 1,
        |     "description": "User already registered"
        |   }
        |}
      """.stripMargin)
  }
}
