package com.equeo.points_test.controllers

import com.equeo.points_test.AuthService
import com.equeo.points_test.domain.User
import com.equeo.points_test.exceptions.UserNotFoundException
import com.equeo.points_test.services.{TokenService, UserService}
import com.twitter.finagle.http.Status
import com.twitter.finatra.http.{EmbeddedHttpServer, HttpTest}
import com.twitter.inject.server.FeatureTest
import org.scalamock.scalatest.MockFactory

class LoginControllerTest extends FeatureTest with MockFactory with HttpTest {

  val userService: UserService = mock[UserService]
  val tokenService: TokenService = mock[TokenService]

  override protected val server: EmbeddedHttpServer =
    new EmbeddedHttpServer(new AuthService)
      .bind[UserService](userService)
      .bind[TokenService](tokenService)

  test("success login") {
    val mockUser = User(5, "test", "some pass")
    (userService.getUserByUsername _)
      .expects("test")
      .returns(mockUser)
    (userService.checkPassword _)
      .expects(mockUser, "password")
      .returns(true)
    (tokenService.generateAndSaveToken _)
      .expects(5)
      .returns("woqpc32fe")

    server.httpPost(path = "/auth/login", postBody =
      """
        |{
        |   "username": "test",
        |   "password": "password"
        |}
      """.stripMargin, andExpect = Status.Ok, withJsonBody =
      """
        |{
        |   "success": true,
        |   "response":
        |   {
        |     "user": {
        |       "id": 5,
        |       "username": "test"
        |     },
        |     "token": "woqpc32fe"
        |   }
        |}
      """.stripMargin)
  }

  test("login with wrong password") {
    val mockUser = User(5, "test", "some pass")
    (userService.getUserByUsername _)
      .expects("test")
      .returns(mockUser)
    (userService.checkPassword _)
      .expects(mockUser, "password")
      .returns(false)

    server.httpPost(path = "/auth/login", postBody =
      """
        |{
        |   "username": "test",
        |   "password": "password"
        |}
      """.stripMargin, andExpect = Status.BadRequest, withJsonBody =
      """
        |{
        |   "success": false,
        |   "error":
        |   {
        |     "code": 2,
        |     "description": "Wrong username or password"
        |   }
        |}
      """.stripMargin)
  }

  test("login with wrong username") {
    (userService.getUserByUsername _)
      .expects("test")
      .throws(new UserNotFoundException("test message"))

    server.httpPost(path = "/auth/login", postBody =
      """
        |{
        |   "username": "test",
        |   "password": "password"
        |}
      """.stripMargin, andExpect = Status.BadRequest, withJsonBody =
      """
        |{
        |   "success": false,
        |   "error":
        |   {
        |     "code": 2,
        |     "description": "Wrong username or password"
        |   }
        |}
      """.stripMargin)
  }

}
