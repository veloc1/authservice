package com.equeo.points_test.services

import com.equeo.points_test.UnitSpec
import com.equeo.points_test.data.UserDao
import com.equeo.points_test.domain.User

class UserServiceTest extends UnitSpec {

  trait Service {
    val userDao: UserDao = mock[UserDao]
    val service: UserService = new UserService(userDao)
  }

  "A service" should "be able to register user" in new Service {
    val user = User(0, "Test", "Pass")

    (userDao.saveUser _).expects(user)

    service.register(user)
  }

  it should "be able to check user password" in new Service {
    private val user1 = User(1, "test", "pass1")
    private val user2 = User(2, "test2", "pass2")

    var verified: Boolean = service.checkPassword(user1, "pass1")
    assert(verified)

    verified = service.checkPassword(user2, "pass1")
    assert(!verified)
  }

  it should "be able to request user from dao" in new Service {
    val user1 = User(1, "test", "pass1")
    (userDao.getUserByUsername _)
      .expects("test")
      .returns(user1)
    val user2: User = service.getUserByUsername("test")
    assert(user1 == user2)
  }

}
