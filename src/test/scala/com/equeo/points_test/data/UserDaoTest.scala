package com.equeo.points_test.data

import com.equeo.points_test.UnitSpec
import com.equeo.points_test.domain.User
import com.equeo.points_test.exceptions.{UserAlreadyRegisteredException, UserNotFoundException}

class UserDaoTest extends UnitSpec {

  trait Dao {
    val dao: UserDao = new UserDao
  }

  "A dao" should "be able to register user and return this user" in new Dao {
    val user = User(0, "test", "pass")
    val createdUser: User = dao.saveUser(user)
    assert(createdUser.username == "test")
  }

  it should "throw UserAlreadyRegisteredException if user already registered" in new Dao {
    val user = User(0, "test2", "pass")
    dao.saveUser(user)
    assertThrows[UserAlreadyRegisteredException] {
      dao.saveUser(user)
    }
  }

  it should "change id on every new item" in new Dao {
    val user1 = User(0, "test", "pass")
    val savedUser1: User = dao.saveUser(user1)
    val user2 = User(0, "test2", "pass")
    val savedUser2: User = dao.saveUser(user2)
    assert(savedUser1.id != savedUser2.id)
  }

  it should "be able to get user by login and id" in new Dao {
    val user = User(0, "test", "pass")
    val createdUser: User = dao.saveUser(user)
    val getUser1: User = dao.getUserByUsername(user.username)
    assert(getUser1.username == createdUser.username)
    val getUser2: User = dao.getUserById(createdUser.id)
    assert(getUser2.id == createdUser.id)
  }

  it should "throw UserNotFound if wrong id specified" in new Dao {
    assertThrows[UserNotFoundException] {
      dao.getUserById(-123)
    }
  }

  it should "return null if wrong login specified" in new Dao {
    val createdUser: User = dao.getUserByUsername("test_login_unused")
    assert(createdUser == null)
  }

  it should "return correct item when called getUserById or getUserByLogin" in new Dao {
    (1 to 5).foreach((index: Int) => {
      dao.saveUser(User(index, s"test${index - 1}", s"pass${index - 1}"))
    })
    val getUser1: User = dao.getUserById(3)
    assert(getUser1.id == 3)
    assert(getUser1.username == "test3")

    val getUser2: User = dao.getUserByUsername("test4")
    assert(getUser2.id == 4)
    assert(getUser2.username == "test4")
  }
}
