package com.equeo.points_test.data

import com.equeo.points_test.UnitSpec
import com.equeo.points_test.domain.Token

class TokenDaoTest extends UnitSpec {

  trait Dao {
    val dao: TokenDao = new TokenDao
  }

  "A dao" should "be able to save token" in new Dao {
    val token = Token(0, "lpdf", 1)
    val createdToken: Token = dao.saveToken(token)
    assert(createdToken.token == "lpdf")
    assert(createdToken.user == 1)
  }

  it should "be able to return list of tokens assigned to user" in new Dao {
    val token1 = Token(0, "lpdf", 1)
    val token2 = Token(0, "fpsd", 2)
    val token3 = Token(0, "aspd", 1)
    val token4 = Token(0, "as23", 3)
    dao.saveToken(token1)
    dao.saveToken(token2)
    dao.saveToken(token3)
    dao.saveToken(token4)

    val tokens: List[Token] = dao.getTokensByUser(1)

    assert(tokens.length == 2)
    tokens.filter((token: Token) => {
      token.user == 1 && (token.token == "lpdf" || token.token == "aspd")
    })
  }
}
