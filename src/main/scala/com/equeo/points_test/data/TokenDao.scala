package com.equeo.points_test.data

import com.equeo.points_test.domain.Token

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Data Access Object. Currently implemented as memory based storage. Not singleton.
  */
class TokenDao {

  var tokens: mutable.Buffer[Token] = ArrayBuffer.empty[Token]
  var lastId: Integer = 0

  def saveToken(token: Token): Token = {
    val newToken = token.copy(id = lastId)
    tokens += newToken
    lastId += 1
    newToken
  }

  def getTokensByUser(userId: Int): List[Token] = {
    tokens
      .filter((token: Token) => {
        token.user == userId
      })
      .toList
  }
}
