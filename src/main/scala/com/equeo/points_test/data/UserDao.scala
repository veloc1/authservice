package com.equeo.points_test.data

import com.equeo.points_test.domain.User
import com.equeo.points_test.exceptions.{UserAlreadyRegisteredException, UserNotFoundException}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Data Access Object. Currently implemented as memory based storage. Not singleton. Doesn't hash password.
  */
class UserDao {

  var users: mutable.Buffer[User] = ArrayBuffer.empty[User]
  var lastId: Integer = 0

  @throws(classOf[UserAlreadyRegisteredException])
  def saveUser(user: User): User = {
    val locals = users
      .filter((local: User) => local.username == user.username)

    if (locals.isEmpty) {
      val newUser = user.copy(id = lastId)
      users += newUser
      lastId += 1
      newUser
    } else {
      throw new UserAlreadyRegisteredException(
        s"User with username ${user.username} already registered")
    }
  }

  def getUserById(id: Integer): User = {
    // users.filter((user: User) => user.id == id).head
    // or
    getUserById(id, users) // this way more elixir'ish
  }

  def getUserByUsername(login: String): User = {
    getUserByUsername(login, users)
  }

  private def getUserById(id: Integer, users: mutable.Buffer[User]): User = users.length match {
    case 0 =>
      throw new UserNotFoundException(s"User with id: $id not found")
    case _ =>
      if (users.head.id == id) {
        users.head
      } else {
        getUserById(id, users.tail)
      }
  }

  private def getUserByUsername(login: String, users: mutable.Buffer[User]): User = users.length match {
    case 0 =>
      throw new UserNotFoundException(s"User with username: $login not found")
    case _ =>
      if (users.head.username == login) {
        users.head
      } else {
        getUserByUsername(login, users.tail)
      }
  }
}
