package com.equeo.points_test

import com.equeo.points_test.controllers.{LoginController, RegisterController}
import com.equeo.points_test.exceptions.mappers.{UserAlreadyRegisteredExceptionMapper, UserNotFoundExceptionMapper, LogAllExceptionsExceptionMapper}
import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.CommonFilters
import com.twitter.finatra.http.routing.HttpRouter

/**
  * Simple server for testing authorization.
  *
  * @see RegisterController
  */
object Main extends AuthService

class AuthService extends HttpServer {

  override def configureHttp(router: HttpRouter) {
    router
      .filter[CommonFilters]
      .exceptionMapper[LogAllExceptionsExceptionMapper]
      .exceptionMapper[UserAlreadyRegisteredExceptionMapper]
      .exceptionMapper[UserNotFoundExceptionMapper]
      .add[RegisterController]
      .add[LoginController]
  }
}
