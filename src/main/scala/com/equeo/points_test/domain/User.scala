package com.equeo.points_test.domain

case class User(id: Int, username: String, password: String)
