package com.equeo.points_test.domain.http

case class GenericError(code: Int, description: String)

object GenericError {

  def UserAlreadyRegistered: GenericError = {
    GenericError(1, "User already registered")
  }

  def InvalidCredentials: GenericError = {
    GenericError(2, "Wrong username or password")
  }

}
