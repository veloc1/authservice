package com.equeo.points_test.domain.http

import com.equeo.points_test.domain.User

case class RegisterResponse(user: User, token: String)
