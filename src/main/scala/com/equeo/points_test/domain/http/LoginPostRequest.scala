package com.equeo.points_test.domain.http

import com.twitter.finatra.validation.{NotEmpty, Size}


case class LoginPostRequest(@NotEmpty username: String, @Size(min = 6, max = 128) password: String)
