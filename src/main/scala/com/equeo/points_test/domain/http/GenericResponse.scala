package com.equeo.points_test.domain.http

case class GenericResponse[RESPONSE]
(
  success: Boolean,
  response: RESPONSE,
  error: GenericError
)

object GenericResponse {
  def success[R](data: R): GenericResponse[R] = {
    GenericResponse(success = true, data, null)
  }

  def error(error: GenericError): GenericResponse[Any] = {
    GenericResponse(success = false, null, error)
  }
}
