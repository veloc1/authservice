package com.equeo.points_test.domain

case class Token(id: Int, token: String, user: Int)
