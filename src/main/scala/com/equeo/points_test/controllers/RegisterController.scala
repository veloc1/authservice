package com.equeo.points_test.controllers

import javax.inject.Inject

import com.equeo.points_test.domain.User
import com.equeo.points_test.domain.http.{GenericResponse, RegisterPostRequest, RegisterResponse}
import com.equeo.points_test.services.{TokenService, UserService}
import com.twitter.finatra.http.Controller

class RegisterController @Inject()(
                                    userService: UserService,
                                    tokenService: TokenService)
  extends Controller {

  post("/auth/register") { request: RegisterPostRequest =>
    val user = User(0, request.username, request.password)

    // if we have an exception,
    // then it will processed by `UserAlreadyRegisteredExceptionMapper`
    val createdUser: User = userService.register(user).copy(password = null)

    val token = tokenService.generateAndSaveToken(createdUser.id)

    GenericResponse.success(RegisterResponse(user = createdUser, token = token))
  }
}
