package com.equeo.points_test.controllers

import javax.inject.Inject

import com.equeo.points_test.domain.User
import com.equeo.points_test.domain.http.{GenericError, GenericResponse, LoginPostRequest, LoginResponse}
import com.equeo.points_test.services.{TokenService, UserService}
import com.twitter.finatra.http.Controller

class LoginController @Inject()(
                                 userService: UserService,
                                 tokenService: TokenService)
  extends Controller {

  post("/auth/login") { request: LoginPostRequest =>
    val user: User = userService.getUserByUsername(request.username)
    val isPasswordValid = userService.checkPassword(user, request.password)
    if (isPasswordValid) {
      val token: String = tokenService.generateAndSaveToken(user.id)
      GenericResponse.success(
        LoginResponse(
          token = token,
          user = user.copy(password = null)))
    } else {
      response
        .badRequest(GenericResponse.error(GenericError.InvalidCredentials))
        .contentType(response.jsonContentType)
    }
  }
}
