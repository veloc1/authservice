package com.equeo.points_test.exceptions

class UserNotFoundException(message: String) extends Exception