package com.equeo.points_test.exceptions.mappers

import javax.inject.Inject

import com.equeo.points_test.domain.http.{GenericError, GenericResponse}
import com.equeo.points_test.exceptions.UserNotFoundException
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder


class UserNotFoundExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[UserNotFoundException] {

  override def toResponse(request: Request, throwable: UserNotFoundException): Response = {
    throwable.printStackTrace()

    response
      .badRequest(GenericResponse.error(GenericError.InvalidCredentials))
      .contentType(response.jsonContentType)
  }
}