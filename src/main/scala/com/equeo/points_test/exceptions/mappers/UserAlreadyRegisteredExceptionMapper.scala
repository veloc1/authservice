package com.equeo.points_test.exceptions.mappers

import javax.inject.Inject

import com.equeo.points_test.domain.http.{GenericError, GenericResponse}
import com.equeo.points_test.exceptions.UserAlreadyRegisteredException
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder


class UserAlreadyRegisteredExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[UserAlreadyRegisteredException] {

  override def toResponse(request: Request, throwable: UserAlreadyRegisteredException): Response = {
    throwable.printStackTrace()

    response
      .badRequest(GenericResponse.error(GenericError.UserAlreadyRegistered))
      .contentType(response.jsonContentType)
  }
}