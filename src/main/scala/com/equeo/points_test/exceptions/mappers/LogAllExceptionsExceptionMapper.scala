package com.equeo.points_test.exceptions.mappers

import javax.inject.Inject

import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder


class LogAllExceptionsExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[Throwable] {

  override def toResponse(request: Request, throwable: Throwable): Response = {
    throwable.printStackTrace()
    response.badRequest(throwable.toString)
  }
}