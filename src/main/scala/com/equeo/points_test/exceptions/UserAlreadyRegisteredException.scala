package com.equeo.points_test.exceptions

/**
  * @see [[com.equeo.points_test.data.UserDao]]
  */
class UserAlreadyRegisteredException(message: String) extends Exception
