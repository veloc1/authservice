package com.equeo.points_test.services

import javax.inject.{Inject, Singleton}

import com.equeo.points_test.data.UserDao
import com.equeo.points_test.domain.User
import com.equeo.points_test.exceptions.{UserAlreadyRegisteredException, UserNotFoundException}

/**
  * Service for handling all user operations at business level.
  *
  * @param userDao - storage
  */
@Singleton
class UserService @Inject()(userDao: UserDao) {

  @throws(classOf[UserAlreadyRegisteredException])
  def register(user: User): User = {
    userDao.saveUser(user)
  }

  @throws(classOf[UserNotFoundException])
  def getUserByUsername(username: String): User = {
    userDao.getUserByUsername(username)
  }

  @throws(classOf[UserNotFoundException])
  def checkPassword(user: User, password: String): Boolean = {
    checkPasswordSecurely(user, password)
  }

  private def checkPasswordSecurely(user: User, password: String): Boolean = {
    user.password == password
  }

}
