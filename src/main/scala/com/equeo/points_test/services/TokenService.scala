package com.equeo.points_test.services

import javax.inject.{Inject, Singleton}

import com.equeo.points_test.data.TokenDao
import com.equeo.points_test.domain.Token

import scala.util.Random

@Singleton
class TokenService @Inject()(tokenDao: TokenDao) {

  def generateAndSaveToken(userId: Int): String = {
    val token = Token(0, Random.alphanumeric.take(16).mkString, userId)
    tokenDao.saveToken(token)
    token.token
  }

}
