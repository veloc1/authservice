# AuthService

Simple service for testing purposes with no dependencies. 

Tools used: Scala, sbt, Twitter Finatra, Scalatest.

# Endpoints:
## Register
POST /auth/register

INPUT

	{
		"username": "test",
		"password": "password"
	}
	
OUTPUT

Code 200

	{
   		"success": true,
   		"response":
   		{
     		"user": {
       			"id": 5,
       			"username": "test"
     		},
     		"token": "pxzu382pdshv9z"
   		}
	}
OR

Code 400 

	{
   		"success": false,
   		"error": {
     		"code": 1,
     		"description": "User already registered"
   		}
	}


## Register
POST /auth/login

INPUT

	{
		"username": "test",
		"password": "password"
	}
	
OUTPUT

Code 200

	{
   		"success": true,
   		"response":
   		{
     		"user": {
       			"id": 5,
       			"username": "test"
     		},
     		"token": "woqpc32fe"
   		}
	}

OR

Code 400 

	{
   		"success": false,
   		"error":
   		{
     		"code": 2,
     		"description": "Wrong username or password"
   		}
	}
